default['workstation']['chef-solo-repos-destination'] = '/opt/chef-solo'

# Users to configure:
default['workstation']['users'] = []

default['workstation']['cron']['jobs'] = [
  {
    name: 'chef-solo-run',
    hour: '*',
    minute: '0',
    command: 'cd /opt/chef-solo/ && /opt/chef-solo/run.sh > /var/log/chef-solo.log',
    only_if: ::Dir.exist?('/opt/chef-solo'),
  },
]

default['workstation']['ppa_repositories'] = [
#  {
#      "name": "dpluzz",
#      "uri": "http://ppa.launchpad.net/yoggic/dpluzz/ubuntu",
#      "components": ["main"]
#  }
]
default['workstation']['debian']['repos'] = []

# system packages
default['workstation']['packages'] = %w(
  git
)

default['workstation']['snap_packages'] = []

# Packages to install directly from url
default['workstation']['packages_from_url'] = {
  # 'vernemq' => 'https://bintray.com/artifact/download/erlio/vernemq/deb/bionic/vernemq_1.6.1-1_amd64.deb'
}

default['workstation']['packages_to_remove'] = []

# gem packages
default['workstation']['gems'] = %w(
  bundler
  rake
)

# pip packages
default['workstation']['pip'] = {
  # 'ansible' => '2.3.2.0',
  # 'pyOpenSSL' => '17.5.0',
}

default['workstation']['remote_archives'] = [
#  {
#    source: 'http://foo/bar.tgz',
#    destination: '/usr/local/bin'
#  }
]

default['workstation']['services'] = []
# {
#   name: 'foo',
#   action: [:enabled, :started]
#  }

default['workstation']['scripts'] = []

default['workstation']['symlinks'] = []

# /etc/hosts
default['workstation']['hosts'] = []

# SSH config:
default['workstation']['ssh_config'] = []
# {
#   name: "foo",
#   options: {},
#   user: "suername"
# }

default['workstation']['customize_git'] = true
# System wide git config :
default['workstation']['git']['owner'] = ""
default['workstation']['git']['config'] = []
default['workstation']['git']['gitignore'] = []

default['workstation']['terminal']['config'] = [
  {
    key: 'FontName',
    value: 'DejaVu Sans Mono 9',
  },
  {
    key: 'MiscAlwaysShowTabs',
    value: 'FALSE',
  },
  {
    key: 'MiscBell',
    value: 'FALSE',
  },
  {
    key: 'MiscBordersDefault',
    value: 'TRUE',
  },
  {
    key: 'MiscCursorBlinks',
    value: 'FALSE',
  },
  {
    key: 'MiscCursorShape',
    value: 'TERMINAL_CURSOR_SHAPE_BLOCK',
  },
  {
    key: 'MiscDefaultGeometry',
    value: '136x62',
  },
  {
    key: 'MiscInheritGeometry',
    value: 'FALSE',
  },
  {
    key: 'MiscMenubarDefault',
    value: 'TRUE',
  },
  {
    key: 'MiscMouseAutohide',
    value: 'FALSE',
  },
  {
    key: 'MiscToolbarDefault',
    value: 'FALSE',
  },
  {
    key: 'MiscConfirmClose',
    value: 'TRUE',
  },
  {
    key: 'MiscCycleTabs',
    value: 'TRUE',
  },
  {
    key: 'MiscTabCloseButtons',
    value: 'TRUE',
  },
  {
    key: 'MiscTabCloseMiddleClick',
    value: 'TRUE',
  },
  {
    key: 'MiscTabPosition',
    value: 'GTK_POS_TOP',
  },
  {
    key: 'MiscHighlightUrls',
    value: 'TRUE',
  },
  {
    key: 'MiscScrollAlternateScreen',
    value: 'TRUE',
  },
  {
    key: 'ScrollingLines',
    value: '500000',
  },
  {
    key: 'ColorBackground',
    value: '#080C13',
  },
  {
    key: 'ColorForeground',
    value: '#EFEFEF',
  },
]

default['workstation']['touchpad'] = :disabled

default['workstation']['ntp']['server'] = "0.ca.pool.ntp.org 1.ca.pool.ntp.org 2.ca.pool.ntp.org 3.ca.pool.ntp.org"

default['workstation']['npm_packages'] = {
  # 'name' => 'version'
}

default['workstation']['yarn_packages'] = []

default['workstation']['xinput'] = {
  # 'device_name' => 'Kensington Expert Wireless TB',
  # 'button_map' => ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
}
