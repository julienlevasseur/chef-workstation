class Chef
  class Recipe
    def get_script_path(destination)
      path = destination.split('/')
      path.shift
      path.pop
      x = '/'
      path.each do |e|
        x = x + e + '/'
      end
      Chef::Log.debug(x)
      return x
    end

    def pip_package_installed?(package_name)
      cmd = Mixlib::ShellOut.new("pip list --format=columns|grep #{package_name}")
      # return true if system "pip list --format=columns|grep #{package_name}"
      return true if cmd.run_command
    end

    def xinput_device_id(device_name)
      cmd = Mixlib::ShellOut.new("/usr/bin/xinput|grep \"#{device_name}\"|head -1|awk '{print $7}'|cut -d '=' -f 2")
      cmd.run_command
      return cmd.stdout.delete!("\n")
    end

    def xinput_set_button_map(device, button_map)
      cmd = Mixlib::ShellOut.new("/usr/bin/xinput set-button-map #{device} #{button_map}")
      cmd.run_command
    end
  end
end
