#
# Cookbook:: workstation
# Recipe:: default
#
# Copyright:: 2017, Julien Levasseur, All Rights Reserved.

# Get the chef-solo package to run it via cron
# git node['workstation']['chef-solo-repos-destination'] do
#   repository 'git@bitbucket.org:nukuloa/chef-solo_playground.git'
#   revision 'master'
#   action :checkout
# end

tag('workstation')

if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
  include_recipe 'workstation::debian' if node['platform_family'] == 'debian'
  include_recipe 'workstation::users'
end

# Install packages :
node['workstation']['packages'].each do |pkg|
  package pkg
end

if node['platform'] == 'mac_os_x'
  node['workstation']['packages_from_url'].each do |pkg_name, pkg_url|
    dmg_package pkg_name do
      source   pkg_url
      action   :install
    end
  end

  file '/var/root/.profile' do
    content 'bash'
    mode 0644
    owner 'root'
  end
end

node['workstation']['packages_to_remove'].each do |pkg|
  package pkg do
    action :remove
  end
end

node['workstation']['gems'].each do |g|
  gem_package g do
    action :install
    not_if "gem list|grep #{g}"
  end
end

include_recipe 'workstation::nodejs'

node['workstation']['services'].each do |service|
  actions = []
  service['action'].each do |action|
    actions.push(action.to_sym)
  end
  service service['name'] do
    action actions
  end
end

directory '/opt/scripts'

node['workstation']['scripts'].each do |script|
  directory get_script_path(script['destination']) do
    recursive true
  end
  remote_file script['destination'] do
    source script['source']
    mode script['mode']
  end
end

node['workstation']['remote_archives'].each do |archive|
  directory archive['destination'] do
    action :create
    recursive true
  end

  tar_extract archive['source'] do
    target_dir archive['destination']
  end
end

node['workstation']['symlinks'].each do |link|
  link link['from'] do
    to link['to']
  end
end

# template '/etc/hosts' do
#   source 'hosts.erb'
#   owner 'root'
#   group 'root'
#   mode '0644'
#   variables(
#     hosts: node['workstation']['hosts']
#   )
# end

include_recipe 'workstation::git' if node['workstation']['customize_git'] == true
# include_recipe 'consul-template::default'

node['workstation']['ssh_config'].each do |ssh_config|
  ssh_config ssh_config['name'] do
    options ssh_config['options']
    user ssh_config['user']
  end
end

# Vim setup:
git '/opt/Config-files' do
  repository 'https://gitlab.com/julienlevasseur/Config-files.git'
  revision 'master'
  action :checkout
end

execute 'vim_setup' do
  cwd '/opt/Config-files'
  command 'cd /opt/Config-files && /opt/Config-files/setup-vim.sh && touch /opt/Config-files/vim_setuped && cd -'
  not_if { node['virtualization']['system'] == 'docker' }
  not_if { ::File.exist?('/opt/Config-files/vim_setuped') }
  # not_if tagged?('vim_setuped')
end
tag('vim_setuped')


node['workstation']['users'].each do |user|
  if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
    directory "/home/#{user['name']}/.config" do
      owner user['name']
      group user['group']
      recursive true
    end

    directory "/home/#{user['name']}/.vim/pack/plugins/start/vimwiki" do
      recursive true
    end

    git "/home/#{user['name']}/.vim/pack/plugins/start/vimwiki" do
      repository 'https://github.com/vimwiki/vimwiki.git'
      revision 'master'
      action :checkout
    end

    file "/home/#{user['name']}/.vimrc" do
      owner user['name']
      group user['group']
    end

    directory "/home/#{user['name']}/.vim" do
      owner user['name']
      group user['group']
      recursive true
    end
  elsif node['platform'] == 'mac_os_x'
    directory "/Users/#{user['name']}/.config" do
      owner user['name']
      group user['group']
      recursive true
    end

    directory "/Users/#{user['name']}/.vim/pack/plugins/start/vimwiki" do
      recursive true
    end

    git "/Users/#{user['name']}/.vim/pack/plugins/start/vimwiki" do
      repository 'https://github.com/vimwiki/vimwiki.git'
      revision 'master'
      action :checkout
    end

    file "/Users/#{user['name']}/.vimrc" do
      owner user['name']
      group user['group']
    end

    directory "/Users/#{user['name']}/.vim" do
      owner user['name']
      group user['group']
      recursive true
    end
  end
end

node.save if node['virtualization']['system'] == 'docker' # ~FC075

# Disable Touchpad:
if node['workstation']['touchpad'] == :disabled
  execute 'disable_touchpad' do
    command 'synclient TouchpadOff=1'
    only_if { ::File.exist?('/usr/bin/synclient') }
    only_if 'synclient|grep TouchpadOff|grep 0'
  end
end

if node['workstation']['xinput'].length() > 0
  package 'xinput'
  node['workstation']['xinput'].each do |device_name, button_map|
    log 'device_name' do
      message "#{device_name}"
      level :info
    end
    log 'button_map' do
      message "#{button_map}"
      level :info
    end
    #xinput_set_button_map(xinput_device_id(device_name), button_map.join(' '))
    xinput_set_button_map(xinput_device_id(device_name), button_map)
  end
end
