#
# Cookbook:: workstation
# Recipe:: users
#
# Copyright:: 2017, Julien Levasseur, All Rights Reserved.

node['workstation']['users'].each do |user|
  if Chef::Config[:file_cache_path].include?('kitchen')
    user user['name']# do

    directory "#{user['home']}/.ssh" do
      owner user['name']
      group user['name']
      mode '0755'
      recursive true
      action :create
    end
  end

  # SSH keys :
  ssh_key 'id_rsa' do # ~FC022
    path "#{user['home']}/.ssh"
    not_if { ::File.exist?("#{user['home']}/.ssh/id_rsa") }
  end

  # Terminal customization:
  directory "#{user['home']}/.config/xfce4/terminal" do
    owner user['name']
    group user['name']
    mode 0755
    action :create
    recursive true
  end

  template "#{user['home']}/.config/xfce4/terminal/terminalrc" do
    source 'terminalrc.erb'
    owner user['name']
    group user['name']
    mode 0644
    variables(
      config: node['workstation']['terminal']['config']
    )
  end

  sudo 'password-less-access' do
    user user['name']
    nopasswd true
  end

  # Give access to docker to user:
  execute "Give access to Docker to #{user}['name']" do
    command "usermod -a -G docker #{user['name']}"
    only_if "grep docker /etc/group"
    not_if "grep docker /etc/group|grep #{user}['name']"
    not_if { node['virtualization']['system'] == 'docker' }
  end

  # Set user in vboxsf group if the workstation is a VBox VM:
  execute "Set #{user['name']} a vboxsf group member" do
    command "usermod -a -G vboxsf #{user['name']}"
    only_if "grep vboxsf /etc/group"
    not_if "grep vboxsf /etc/group|grep #{user['name']}"
    only_if { node['virtualization']['system'] == 'vbox' }
  end
end
