#
# Cookbook:: chef-workstation
# Recipe:: git
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

node['workstation']['git']['config'].each do |gitconfig|
  git_config 'change system config' do
    scope 'system'
    key gitconfig['key']
    value gitconfig['value']
  end

  if gitconfig['key'] == 'core.excludesfile'
    template gitconfig['value'] do
      source 'gitignore.erb'
      owner node['workstation']['git']['owner']
      group 'staff'
      mode '0644'
      variables(
        gitignore: node['workstation']['git']['gitignore']
      )
    end
  end
end
