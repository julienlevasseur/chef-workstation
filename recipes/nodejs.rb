#
# Cookbook:: chef-workstation
# Recipe:: nodejs
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

include_recipe "nodejs"

node['workstation']['npm_packages'].each do |pkg, pkg_version|
  npm_package pkg do
    version pkg_version
  end
end

node['workstation']['yarn_packages'].each do |pkg|
  execute "execute yarn add #{pkg}" do
    command "yarn add #{pkg}"
    action :run
    only_if 'dpkg -l|grep yarn'
    not_if "yarn list|grep #{pkg}"
  end
end
