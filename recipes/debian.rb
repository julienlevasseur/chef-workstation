#
# Cookbook:: workstation
# Recipe:: ubuntu
#
# Copyright:: 2017, Julien Levasseur, All Rights Reserved.

directory '/tmp/chef' # Used to download packages from url

apt_update

#package 'python-pip'
#python_runtime '2'
#node['workstation']['pip'].each do |package, version|
#  python_package package do
#    version version
#  end
#end

package 'xinput'

node['workstation']['cron']['jobs'].each do |cronjob|
  cron cronjob['name'] do # ~FC022
    hour cronjob['hour']
    minute cronjob['minute']
    command cronjob['command']
    only_if { cronjob['only_if'] } if cronjob['only_if']
    not_if { node['virtualization']['system'] == 'docker' }
  end
end

node['workstation']['snap_packages'].each do |pkg_name|
  snap_package pkg_name do
    action :install
    not_if "snap list|grep #{pkg_name}"
  end
end

node['workstation']['packages_from_url'].each do |pkg_name, pkg_url|
  remote_file "/tmp/chef/#{pkg_name}" do
    source pkg_url
    mode 0644
    not_if "dpkg -l|grep #{pkg_name}"
  end

  dpkg_package pkg_name do
    source "/tmp/chef/#{pkg_name}"
    action :install
    not_if "dpkg -l|grep #{pkg_name}"
  end

  file "/tmp/chef/#{pkg_name}" do
    action :delete
  end
end

node['workstation']['ppa_repositories'].each do |repo|
  apt_repository repo['name'] do
    uri repo['uri']
    components repo['components']
    key repo['key']
    action :add
  end
end
